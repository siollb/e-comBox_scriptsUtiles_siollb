#!/bin/bash

# Ce script intéractif fournit un modèle pour construire directement une image.
# Il est prévu ici qu'il s'exécute sur le serveur Docker sur lequel tous les dépôts 
# sont installés dans /home/sio/appli_ecombox/.
# Il fonctionne actuellement pour Prestashop, Woocommerce et blog.
# Il doit permettre également de sauvegarder deux versions anciennes pour chaque image.
# Il doit être adapté pour les autres applications ou si on veut l'exécuter sur une autre poste.


# Définition des variables
CONTEXTE_DOCKERFILE="/home/tech1/appli_ecomBox/e-comBox_dockerfile_siollb"

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLSTOP="\033[1;31m"  # Rouge
COLINFO="\033[0;36m"    # Cyan


STOPPER()
{
        echo -e "$COLSTOP"
        echo -e "STOP! Vous avez décidé de ne pas créer de nouvelle image. Vous pouvez reprendre la procédure quand vous voulez."
        echo -e "$COLTXT"
        exit 1
}



POURSUIVRE()
{
        REPONSE=""
        while [ "$REPONSE" != "o" -a "$REPONSE" != "O" -a "$REPONSE" != "n" ]
        do
          echo -e "$COLTXT"
	  echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
	  read REPONSE
          if [ -z "$REPONSE" ]; then
	     REPONSE="o"
	  fi
        done
        if [ "$REPONSE" != "o" -a "$REPONSE" != "O" ]; then
	   STOPPER
	fi
}


echo -e "$COLTXT"
echo -e "Saisissez le complément du nom du site donné (en minuscule) à partir duquel vous voulez créer l'image : $COLSAISIE\c"
read SUFFIXE

# Menu à afficher
echo -e "$COLTXT"
PS3="Saisir le numéro de l'application dont vous voulez créer l'image ou q pour quitter : "
echo -e "##### Liste des applications disponibles #####"

select APPLI in prestashop-vierge prestashop-art woocommerce-vierge woocommerce-art blog
do
  echo -e "Vous avez choisi l'item $REPLY : $APPLI"
  case "$REPLY" in
    q|Q)echo -e "$COLTXT"
        echo -e "Fermeture du programme." 
        STOPPER;;
    *) if [ -z $APPLI ]
        then
          echo "Erreur de saisie. Veuillez recommencer."
          else
            if [ "$APPLI" = "prestashop-vierge" ]; then
               APPLI=prestashop               
            fi
            if [ "$APPLI" = "woocommerce-vierge" ]; then
               APPLI=woocommerce               
            fi
            NOMBD=${APPLI}
            VOLUME=${APPLI}${SUFFIXE}_${APPLI}_data
            CONTENEURBD=${APPLI}-db-${SUFFIXE}
            if [ "$APPLI" = "prestashop-art" ]; then
               NOMBD=prestashop
               VOLUME=prestashopart${SUFFIXE}_prestashop_data
               CONTENEURBD=prestashop-db-art-${SUFFIXE}
               rm -rf /var/lib/docker/volumes/$VOLUME/_data/var/cache/prod/*
            fi
            if [ "$APPLI" = "woocommerce-art" ]; then
               NOMBD=wordpress
               VOLUME=woocommerceart${SUFFIXE}_woocommerce_data
               CONTENEURBD=woocommerce-db-art-${SUFFIXE}
            fi
            if [ "$APPLI" = "prestashop" ]; then
               NOMBD=prestashop 
               rm -rf /var/lib/docker/volumes/$VOLUME/_data/var/cache/prod/*
            fi
            if [ "$APPLI" = "woocommerce" -o "$APPLI" = "blog" ]; then
               NOMBD=wordpress               
            fi
            #if [ "$APPLI" = "odoo" ]; then
            #   NOMBD=postgres               
            #fi
            echo -e "$COLINFO"
            echo -e "Le volume contenant les sources est $VOLUME."
            echo -e "Le conteneur de la base de données est $CONTENEURBD."
            echo -e "Vous vous apprêtez à créer une image à partir du site $APPLI-$SUFFIXE"
            POURSUIVRE
            
            # Déplacement dans les données du volume des sources du site Web
            cd /var/lib/docker/volumes/$VOLUME/_data
            
            # Sauvegarde des sources dans le contexte du dockerfile
            tar -czf $CONTEXTE_DOCKERFILE/$APPLI/$APPLI/$APPLI-data.tar.gz .[^.]* *
            
            # Test de la réussite de la dernière commande
            if [ $? = 0 ]; then
               echo -e "$COLINFO"
               echo -e "Les données ont été archivées : $CONTEXTE_DOCKERFILE/$APPLI/$APPLI/$APPLI-data.tar.gz"
               else
                echo -e "$COLSTOP"
                echo -e "La commande d'archivage a généré une erreur"
            fi 
            
            # Sauvegarde de la base de données dans le contexte du dockerfile
            # Récupération du nom d'utilisateur et du MDP
            NOM_USER=`docker exec $CONTENEURBD env | grep MYSQL_USER | cut -d"=" -f2`
            MDP=`docker exec $CONTENEURBD env | grep MYSQL_PASSWORD | cut -d"=" -f2`
            # Récupération du fichier SQL
	    echo -e "Logs ConteneurBd $CONTENEURBD Nom = $NOM_USER Mdp = $MDP nombdd $NOMBD"
            docker exec $CONTENEURBD mysqldump -u $NOM_USER -p$MDP $NOMBD > $CONTEXTE_DOCKERFILE/$APPLI/mariadb/$APPLI-db.sql

            # Test de la réussite de la dernière commande
            if [ $? = 0 ]; then
               echo -e "$COLINFO"
               echo -e "Le script SQL de la base de données a été sauvegardé : $CONTEXTE_DOCKERFILE/$APPLI/mariadb/$APPLI-db.sql"
               else
                echo -e "$COLSTOP"
                echo -e "La commande de sauvegarde de la base de données a généré une erreur"
            fi
            
                        
            # Création de l'image
            echo ""
            echo -e "$COLTXT"
            echo -e "L'image va être créé"           
            cd $CONTEXTE_DOCKERFILE/$APPLI/
            docker-compose build
            
            # Test de la réussite de la dernière commande
            if [ $? = 0 ]; then
               echo -e "$COLINFO"
               echo -e "Les images ont été créés. Il faut maintenant les tester via l'application e-comBox."
               else
                echo -e "$COLSTOP"
                echo -e "La commande de création de l'image a généré une erreur"
            fi        
            
            exit 0 
        fi
      ;;
   esac
done




