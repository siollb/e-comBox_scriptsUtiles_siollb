CONTEXTE="/home/tech1/appli_ecomBox"

cd $CONTEXTE/e-comBox_app_siollb
git pull
rm -rf $CONTEXTE/e-comBox_image_siollb/dist
cp -r $CONTEXTE/e-comBox_app_siollb/dist $CONTEXTE/e-comBox_image_siollb/
cd $CONTEXTE/e-comBox_image_siollb/
# Sauvegarde de l'image actuelle
sudo docker tag reseaucerta/e-combox:siollb reseaucerta/e-combox:sauv
sudo docker build -t reseaucerta/e-combox:siollb .
docker push reseaucerta/e-combox:siollb
